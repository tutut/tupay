__version__ = '2.0.3'


import os
import tornado.web
import tornado.options
import tornado.web
from logging import getLogger

log = getLogger('tupay')


class url(object):
    def __init__(self, url):
        self.url = url

    def __call__(self, cls):
        application.add_handlers(
            r'.*$',
            (tornado.web.url(self.url, cls, name=cls.__name__),)
        )
        return cls


class Route(tornado.web.RequestHandler):
    @property
    def log(self):
        return log

    @property
    def builtin_themes_dir(self):
        return os.path.join(
                os.path.dirname(__file__), 'themes')

    @property
    def themes_dir(self):
        return os.path.join(
            self.application.tupay_dir, 'themes')

    def get_theme_dir(self, theme):
        if theme.startswith('built-in-'):
            return os.path.join(
                self.builtin_themes_dir, theme[len('built-in-'):])
        return os.path.join(
            self.themes_dir, theme)


# Imported from executable
if hasattr(tornado.options.options, 'debug'):
    application = tornado.web.Application(
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        debug=tornado.options.options.debug,
        static_url_prefix='%s/static/' % (
            '/%s' % tornado.options.options.uri_root_path
            if tornado.options.options.uri_root_path else '')
    )

    import tupay.routes
