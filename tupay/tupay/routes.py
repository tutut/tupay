import os
import sys
import tornado.options
import tornado.process
import tornado.escape
import tornado.web
import tornado.websocket
from mimetypes import guess_type
from collections import defaultdict
from tupay import url, Route, utils, __version__
from tupay.terminal import Terminal


def u(s):
    if sys.version_info[0] == 2:
        return s.decode('utf-8')
    return s


@url(r'/(?:user/(.+))?/?(?:wd/(.+))?/?(?:session/(.+))?')
class Index(Route):
    def get(self, user, path, session):
        return self.render('index.html')
        #return self.render('index.html')


@url(r'/ws'
     '(?:/user/(?P<user>[^/]+))?/?'
     '(?:session/(?P<session>[^/]+))?/?'
     '(?:/wd/(?P<path>.+))?')
class TermWebSocket(Route, tornado.websocket.WebSocketHandler):
    session_history_size = 50000
    # List of websockets per session per user
    # dict: user -> dict: session -> [TermWebSocket]
    sessions = defaultdict(dict)

    # Terminal for session per user
    # dict: user -> dict: session -> Terminal
    terminals = defaultdict(dict)

    # All terminals sockets for systemd socket deactivation
    sockets = []

    # Session history
    history = {}

    def open(self, user, path, session):
        self.session = session
        self.closed = False
        self.secure_user = None

        # Prevent cross domain
        if self.request.headers['Origin'] not in (
                'http://%s' % self.request.headers['Host']):
            self.log.warning(
                'Unauthorized connection attempt: from : %s to: %s' % (
                    self.request.headers['Origin'],
                    self.request.headers['Host']))
            self.close()
            return

        TermWebSocket.sockets.append(self)

        self.log.info('Websocket opened %r' % self)
        self.set_nodelay(True)

        socket = utils.Socket(self.ws_connection.stream.socket)
        opts = tornado.options.options

        #if not opts.unsecure:
        #    user = utils.parse_cert(
        #        self.ws_connection.stream.socket.getpeercert())
        #    assert user, 'No user in certificate'
        try:
                user = utils.User(name=user)
        except LookupError:
                raise Exception('Invalid user in certificate')

        # Certificate authed user
        self.secure_user = user

        if socket.local and socket.user == utils.User():
            # Local to local returning browser user
            self.secure_user = socket.user

        # Handling terminal session
        if session:
            if session in self.user_sessions:
                # Session already here, registering websocket
                self.user_sessions[session].append(self)
                self.write_message('S' + TermWebSocket.history[session])
                # And returning, we don't want another terminal
                return
            else:
                # New session, opening terminal
                self.user_sessions[session] = [self]
                TermWebSocket.history[session] = ''

        terminal = Terminal(
            user, path, session, socket,
            self.request.headers['Host'], self.render_string, self.write)

        terminal.pty()

        if session:
            if not self.secure_user:
                self.log.error(
                    'No terminal session without secure authenticated user'
                    'or local user.')
                self._terminal = terminal
                self.session = None
            else:
                self.log.info('Openning session %s for secure user %r' % (
                    session, self.secure_user))
                self.user_terminals[session] = terminal
        else:
            self._terminal = terminal

    @property
    def user_sessions(self):
        """Return the dict session of socket lists"""
        if not self.secure_user:
            return {}
        return TermWebSocket.sessions[self.secure_user.name]

    @property
    def user_terminals(self):
        """Return the dict session of terminal"""
        if not self.secure_user:
            return {}
        return TermWebSocket.terminals[self.secure_user.name]

    @classmethod
    def close_all(cls, session, user):
        terminals = TermWebSocket.terminals.get(user.name)
        del terminals[session]

        sessions = TermWebSocket.sessions.get(user.name)
        if sessions:
            sockets = sessions[session]
        for socket in sockets[:]:
            socket.on_close()
            socket.close()
        del sessions[session]

    @classmethod
    def broadcast(cls, session, message, user, emitter=None):
        if message[0] == 'S':
            cls.history[session] += message[1:]
        if len(cls.history[session]) > cls.session_history_size:
            cls.history[session] = cls.history[session][
                -cls.session_history_size:]
        sessions = cls.sessions.get(user.name, [])

        for session in sessions[session]:
            try:
                if session != emitter:
                    session.write_message(message)
            except Exception:
                session.log.exception('Error on broadcast')
                session.close()

    def write(self, message):
        if self.session and self.secure_user:
            if message is None:
                TermWebSocket.close_all(self.session, self.secure_user)
            else:
                TermWebSocket.broadcast(
                    self.session, message, self.secure_user)
        else:
            if message is None:
                self.on_close()
                self.close()
            else:
                self.write_message(message)

    def on_message(self, message):
        if self.session and self.secure_user:
            term = self.user_terminals.get(self.session)
            term and term.write(message)
            if message[0] == 'R':
                # Broadcast resize
                TermWebSocket.broadcast(
                    self.session, message, self.secure_user, self)
        else:
            self._terminal.write(message)

    def on_close(self):
        if self.closed:
            return
        self.closed = True
        self.log.info('Websocket closed %r' % self)
        TermWebSocket.sockets.remove(self)
        if self.session:
            self.user_sessions[self.session].remove(self)
        elif hasattr(self, '_terminal'):
            self._terminal.close()
        else:
            self.log.error(
                'Socket with neither session nor terminal %r' % self)
        opts = tornado.options.options
        if opts.one_shot or (
                self.application.systemd and
                not len(TermWebSocket.sockets) and
                not sum([
                    len(sessions)
                    for user, sessions in TermWebSocket.terminals.items()])):
            sys.exit(0)
