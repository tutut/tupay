#!/usr/bin/env python
# *-* coding: utf-8 *-*

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tornado.options
import tornado.ioloop
import tornado.httpserver
import tornado_systemd
import logging
import webbrowser
import uuid
import getpass
import os
import shutil
import stat
import socket
import sys
import pymysql

tornado.options.define("debug", default=False, help="Debug mode")
tornado.options.define("more", default=False,
                       help="Debug mode with more verbosity")
tornado.options.define("unminified", default=False,
                       help="Use the unminified js (for development only)")

tornado.options.define("host", default='localhost', help="Server host")

tornado.options.define("port", default=57575, type=int, help="Server port")
tornado.options.define("one_shot", default=False,
                       help="Run a one-shot instance. Quit at term close")
tornado.options.define("shell", help="Shell to execute at login")
tornado.options.define("motd", default='motd', help="Path to the motd file.")
tornado.options.define("cmd",
                       help="Command to run instead of shell, f.i.: 'ls -l'")
tornado.options.define("unsecure", default=False,
                       help="Running in http")
tornado.options.define("container_name", default='',
                       help="Running container")
tornado.options.define("port_machine", default='',
                       help="Port Machine")
tornado.options.define("ip_machine", default='',
                       help="IP Machine")
tornado.options.define("login", default=False,
                       help="Use login screen at start")
tornado.options.define("force_unicode_width",
                       default=False,
                       help="Force all unicode characters to the same width."
                       "Useful for avoiding layout mess.")
tornado.options.define("generate_certs", default=False,
                       help="Generate tupay certificates")
tornado.options.define("generate_current_user_pkcs", default=False,
                       help="Generate current user pfx for client "
                       "authentication")
tornado.options.define("generate_user_pkcs", default='',
                       help="Generate user pfx for client authentication "
                       "(Must be root to create for another user)")
tornado.options.define("uri_root_path", default='',
                       help="Sets the servier root path: "
                       "example.com/<uri_root_path>/static/")

if os.getuid() == 0:
    ev = os.getenv('XDG_CONFIG_DIRS', '/etc')
else:
    ev = os.getenv(
        'XDG_CONFIG_HOME', os.path.join(
            os.getenv('HOME', os.path.expanduser('~')),
            '.config'))

tupay_dir = os.path.join(ev, 'tupay')
conf_file = os.path.join(tupay_dir, 'tupay.conf')

if not os.path.exists(conf_file):
    try:
        import tupay
        shutil.copy(
            os.path.join(
                os.path.abspath(os.path.dirname(tupay.__file__)),
                'tupay.conf.default'), conf_file)
        print('tupay.conf installed in %s' % conf_file)
    except:
        pass

tornado.options.define("conf", default=conf_file,
                       help="Tupay configuration file. "
                       "Contains the same options as command line.")


# Do it once to get the conf path
tornado.options.parse_command_line()

if os.path.exists(tornado.options.options.conf):
    tornado.options.parse_config_file(tornado.options.options.conf)

# Do it again to overwrite conf with args
tornado.options.parse_command_line()

options = tornado.options.options

for logger in ('tornado.access', 'tornado.application',
               'tornado.general', 'tupay'):
    level = logging.WARNING
    if options.debug:
        level = logging.INFO
        if options.more:
            level = logging.DEBUG
    logging.getLogger(logger).setLevel(level)

log = logging.getLogger('tupay')

host = options.host
port = options.port


def fill_fields(subject):
    subject.C = 'WW'
    subject.O = 'Tupay'
    subject.OU = 'Tupay Terminal'
    subject.ST = 'World Wide'
    subject.L = 'Terminal'


def write(file, content):
    with open(file, 'wb') as fd:
        fd.write(content)
    print('Writing %s' % file)


def read(file):
    print('Reading %s' % file)
    with open(file, 'rb') as fd:
        return fd.read()

def get_lock(process_name):
    global lock_socket   # Without this our lock gets garbage collected
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
        print('Lock Monitoring Memory Process')
    except socket.error:
        print('Lock Exsist')
        sys.exit()

from tupay import application
get_lock(options.container_name+';'+options.ip_machine)
application.tupay_dir = tupay_dir
log.info('Starting server')
http_server = tornado_systemd.SystemdHTTPServer(
    application,
    None)
http_server.listen(port, address=host)

if http_server.systemd:
    os.environ.pop('LISTEN_PID')
    os.environ.pop('LISTEN_FDS')

log.info('Starting loop')

ioloop = tornado.ioloop.IOLoop.instance()

if port == 0:
    port = list(http_server._sockets.values())[0].getsockname()[1]

url = "http://%s:%d/" % (host, port)

if not options.one_shot or not webbrowser.open(url):
    log.warn('Terminal is ready, open your browser to: %s' % url)
db = pymysql.connect(
    host='127.0.0.1', user='root',
    password='root', database='tupay_manage')

cursor = db.cursor()
cursor.execute("UPDATE container SET pid='%s' WHERE ip='%s' AND container='%s'" % (os.getpid(),tornado.options.options.ip_machine,tornado.options.options.container_name))
db.commit()
db.close()

ioloop.start()
