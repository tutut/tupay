#!/opt/python3/bin/python3.5
import psutil

from io import BytesIO
from docker import Client
from hurry.filesize import size
from collections import OrderedDict
import operator

#Create Container

#dockerfile = '''

# Shared Volume
# FROM busybox:buildroot-2014.02

#FROM adhitj/rep_minicapella:centos6.7
#MAINTAINER first last, first.last@yourdomain.com
#VOLUME /data
#CMD ["/bin/sh"]
#'''

#f = BytesIO(dockerfile.encode('utf-8'))
#cli = Client(base_url='tcp://127.0.0.1:2375')

#response = [line for line in cli.build(
#    fileobj=f, rm=True, tag='centos'
#)]
#containers = cli.containers()
#print('\nPulling image response !!')
#for image in range(len(response)):
#    binresponse = response[image].replace(b'\n\n',b'')
#    binresponse = binresponse.replace(b'\n',b'')
#    binresponse = binresponse.replace(b'\u',b'')
#    fixed = binresponse.decode("utf-8")
#    print(fixed)


cli = Client(base_url='tcp://192.168.0.103:3375')
container = cli.containers(all=True, trunc=True)
#containers = sorted(container.items(), key=lambda x:x[1])
containers = sorted(container, key=lambda x: x['Names'])

platform = cli.info()
print('PLATFORM')
print('\nMachine Information')
print('\nMemory Info')
print('Total :',size(psutil.virtual_memory()[0]))
print('Used :',size(psutil.virtual_memory()[3]))
print('Free :',size(psutil.virtual_memory()[4]))

print('\nSwap Info')
print('Total :',size(psutil.swap_memory()[0]))
print('Used :',size(psutil.swap_memory()[1]))
print('Free :',size(psutil.swap_memory()[2]))

print('\nMachine Info')
#print('Platform Machine :',platform['Architecture'])
print('Platform Kernel Version :',platform['KernelVersion'])
print('Platform OS :',platform['OperatingSystem'])
print('Platform Hosts :',platform['Name'])
#print('Platform System :',platform['OSType'])
print('Platform Processor :',platform['NCPU'])

print('\nMachine Information For Container')
print(platform['Name'])
print(platform['ServerVersion']+" |",platform['Driver'])
print(platform['OperatingSystem']+" (",platform['KernelVersion']+" )")
print("Docker Root Dir",platform['DockerRootDir'])
print("CPU",platform['NCPU'],"Core |",size(int(platform['MemTotal']))+" RAM |")
print(platform['Labels'])

print('\nDocker system container !!')
for container in range(len(containers)):
    print('---------------------------------------------------------')
    print('Image Name : ', containers[container]['Names'][0])
    print('---------------------------------------------------------')

    print('Image ID : ',containers[container]['ImageID'])
    #print(containers[container]['Mounts'])
    ports = containers[container]['Ports']
    if len(containers[container]['Ports']) != 0:
       print('Ports :')
       print('  - PrivatePort :',ports[0]['PrivatePort'])
       print('  - Type        :',ports[0]['Type'])
    else:
       pass

    print('Image : ',containers[container]['Image'])
    print('Created : ',containers[container]['Created'])
    #networksettings = containers[container]['NetworkSettings']
    #datanetwork = networksettings['Networks']
    #if 'bridge' in datanetwork:
    #   print('NetworkSettings :')
    #   print('  - Networks Bridge:')
    #   print('     - IPv6Gateway :',datanetwork['bridge']['IPv6Gateway'])
    #   print('     - IPAddress :',datanetwork['bridge']['IPAddress'])
    #   print('     - Links :',datanetwork['bridge']['Links'])
    #   print('     - MacAddress :',datanetwork['bridge']['MacAddress'])
    #   print('     - GlobalIPv6PrefixLen :',datanetwork['bridge']['GlobalIPv6PrefixLen'])
    #   print('     - IPPrefixLen :',datanetwork['bridge']['IPPrefixLen'])
    #   print('     - NetworkID :',datanetwork['bridge']['NetworkID'])
    #   print('     - IPAMConfig :',datanetwork['bridge']['IPAMConfig'])
    #   print('     - EndpointID :',datanetwork['bridge']['EndpointID'])
    #   print('     - GlobalIPv6Address :',datanetwork['bridge']['GlobalIPv6Address'])
    #   print('     - Aliases :',datanetwork['bridge']['Aliases'])
    #else:
    #   pass
    print('Command : ',containers[container]['Command'])
    print('HostConfig :')
    print('  - NetworkMode :',containers[container]['HostConfig']['NetworkMode'])
    print('Status : ',containers[container]['Status'])
    print('Lables : ')
    #print('  - build-date : ',containers[container]['Labels']['build-date'])
    #print('  - vendor : ',containers[container]['Labels']['vendor'])
    #print('  - license : ',containers[container]['Labels']['license'])
    #print('  - name : ',containers[container]['Labels']['name'])
    print('Id : ',containers[container]['Id'],'\n')
