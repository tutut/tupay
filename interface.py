#!/usr/bin/env python3.5
import concurrent.futures, pymysql, os.path, tornado.httpserver, tornado.ioloop, tornado.options, tornado.web, psutil, scann_port, subprocess, time, sys, socket, ast, re

from io import BytesIO
from docker import DockerClient as Client
from hurry.filesize import size
from tornado.options import define, options

define("port", group="root", default=8888, help="run on the given port", type=int)
define("listen_address", group="root", default="172.26.66.108", help="run on the given listen address attach")
define("ip", group="root", default="127.0.0.1", help="run on the given listen address")
define("mysql_host", default="172.26.66.108", help="blog database host")
define("mysql_database", default="tupay_manage", help="tupay database name")
define("mysql_user", default="root", help="tupay database user")
define("root_path", default="/mnt/d/Work/tupay", help="root running programs")
define("mysql_password", default="root", help="tupay database password")

class Application(tornado.web.Application):
    def __init__(self):
            handlers = [
                (r"/", HomeHandler),
                (r"/detail/([^/]+)", DetailHandler),
                (r"/create", CreateHandler),
                (r"/category", CreateCategoryHandler),
                (r"/filter/([^/]+)", FilterHandler),
                (r"/deletecategory/([^/]+)", DeleteCategoryHandler),
                (r"/attach/([^/]+)", AttachHandler),
                (r"/start/([^/]+)", StartHandler),
                (r"/stop/([^/]+)", StopHandler),
		(r"/delete/([^/]+)", DeleteHandler),
                (r".*", ErrorHandler),
            ]

            settings = dict(
                tupay_title=u"Tupay (Docker Management)",
                static_path=os.path.join(os.path.dirname(__file__), "template"),
                template_path=os.path.join(os.path.dirname(__file__), "template"),
                cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
                #debug=True,
                autoreload=True,
                compiled_template_cache=True,
                static_hash_cache=True
            )

            super(Application, self).__init__(handlers, **settings)

            # Have one global connection to the blog DB across all handlers
            try:
                self.db = pymysql.connect(
                    host=options.mysql_host, user=options.mysql_user,
    	            password=options.mysql_password, database=options.mysql_database)
            except Exception:
                sys.exit(1)
            if not handlers:
                self.redirect("/")

def _execute(query):
    connection = pymysql.connect(options.mysql_host,options.mysql_user,options.mysql_password,options.mysql_database)
    cursorobj = connection.cursor()
    try:
       cursorobj.execute(query)
       result = cursorobj.fetchall()
       connection.commit()
    except OperationalError as e:
       connection.rollback()

    connection.close()
    return result

def healthcheck(host,port,fam=socket.AF_INET):
    sock = socket.socket(fam, socket.SOCK_STREAM)
    try:
        result = sock.connect_ex((host,int(port)))
        if result == 0:
            return 'OK'
        else:
            return 'FAIL'
    except:
        pass
    del host,port


repls = {'vo[K':'','[K':'','[01;34m' : '','[01;36m':'','[0m' : '','[m]0;':'','[K[':'','vo[K[K':'','[34;42m':'','[01;32m.':'','[30;42m':'',']0;':'','[?1034h':'','[m':'',""""\'""":'',"\\\\":'',"\\":''}
def multiple_replace(string, rep_dict):
    pattern = re.compile("|".join([re.escape(k) for k in rep_dict.keys()]), re.M)
    return pattern.sub(lambda x: rep_dict[x.group(0)], string)

def join(self):
    self.application.db.ping(True)
    data = _execute("SELECT hosts.id,hosts.ip,hosts.port,category.name,hosts.status FROM hosts INNER JOIN category ON hosts.details=category.id order by category.name")
    return data

class HomeHandler(tornado.web.RequestHandler):
    def get(self):
        # Update all host data
        data_hosts = _execute("SELECT id,ip,port,status,api_container,api_platform FROM hosts order by ip")
        for host in data_hosts:
              if healthcheck(host[1],host[2]) == 'OK':
                 try:
                     cli = Client(base_url='tcp://'+host[1]+':'+str(host[2]))
                     platform = cli.info()
                     containers = cli.containers(all=True, trunc=True)
                     container = sorted(containers, key=lambda x: x['Names'])

                     vcontainer = str(container).replace('\'','\'\'')
                     vplatform = str(platform).replace('\'','\'\'')

                     _execute("UPDATE hosts SET api_container='%s', api_platform='%s', status='%s' where ip='%s'" % (vcontainer,vplatform,'OK',host[1]))
                 except Exception:
                     pass
              else:
                  _execute("UPDATE hosts SET status='%s' where ip='%s'" % ('FAIL',host[1]))

        cli = Client(base_url='tcp://192.168.0.103:3375')
        platform = cli.info()
        print("HASIL >>>",platform)

        machine_info = {
            "KernelMI":platform['KernelVersion'],
            "OSMI":platform['OperatingSystem'],
            "HostsMI":platform['Name'],
            "ProcessorMI":platform['NCPU'],
            "TotalSI":size(psutil.swap_memory()[0]),
            "UsedSI":size(psutil.swap_memory()[0]),
            "FreeSI":size(psutil.swap_memory()[2]),
            "TotalMI":size(psutil.virtual_memory()[0]),
            "UsedMI":size(psutil.virtual_memory()[3]),
            "FreeMI":size(psutil.virtual_memory()[4]),
        }
        data = dict()
        data_hosts = join(self)
        data_cont = dict()
        for host in data_hosts:
            hostip = host[1]
            hostport = host[2]
            print(hostip)
            hostip_container = str(hostip)+'-Container'
            hostip_platform = str(hostip)+'-Platform'

            data_container = _execute("SELECT api_platform,api_container FROM hosts WHERE ip='%s'" % (hostip))
            print("data_container>>>>", data_container)
            container = ast.literal_eval(str(data_container[0][1]))
            platform = ast.literal_eval(data_container[0][0])

            data.update({hostip_container:container})
            data.update({hostip_platform:platform})

        ### Generate data api to mysql if host is down  ###
        for data_host in join(self):
            ip = data_host[1]
            port = str(data_host[2])
            for container in range(len(data[data_host[1]+'-Container'])):
                container_name = str(data[data_host[1]+'-Container'][container]['Names'][0]).replace('/','')
                container_id = str(data[data_host[1]+'-Container'][container]['Id']).replace('/','')
                cursor = self.application.db.cursor()

                data_container = _execute("SELECT * FROM container WHERE ip='%s' AND container='%s' AND container_id='%s'" % (ip,container_name,container_id))

                if not list(data_container):

                    data_check = _execute("SELECT container_id FROM container WHERE ip='%s' AND container='%s'" % (ip,container_name))
                    if data_check:
                       _execute("UPDATE container SET container_id='%s' WHERE ip='%s' AND container='%s'" % (container_id,ip,container_name))
                    else:
                       _execute("INSERT INTO container (ip,container,status,port,pid,container_id) VALUES ('%s','%s','%s','%s','%s','%s')" % (ip,container_name,'deactive',0,0,container_id))
                else:
                    pass

                if data_host[4] == 'OK':
                   cli = Client(base_url='tcp://'+ip+':'+port)
                   platform = cli.info()

                   container = cli.inspect_container(container_id)
                   vcontainer = str(container).replace('\'','\'\'')

                   vplatform = str(platform).replace('\'','\'\'')

                   logs_out = (cli.logs(container_name,tail=50).decode('utf-8'))                   
                   logs_value = multiple_replace(logs_out, repls)

                   vlogs_value = str(logs_value).replace('\'','\'\'')
                   vlogs_value = str(vlogs_value).replace('\"','\'\'')

                   _execute("UPDATE container SET api_container='%s', api_platform='%s', logs_value='%s' where container_id='%s'" % (vcontainer,vplatform,vlogs_value,container_id))
                else:
                   pass


        data_container = _execute("SELECT ip,container,status,port FROM container order by ip")
        for containers in data_container:
            ipcontainer = containers[1]+"ip="+containers[0]
            container = containers[1]
            status = containers[2]
            port = containers[3]
            data_cont.update({ipcontainer:[status,port]})

        self.render("index.html",machine_info=machine_info,data_hosts=data_hosts,data=data,data_cont=data_cont)

class FilterHandler(tornado.web.RequestHandler):
    def get(self,argument):
        # Update all host data with category
        data_hosts = _execute("SELECT id,ip,port,details,status FROM hosts where details = %s" % (argument))
        for host in data_hosts:
            if healthcheck(host[1],host[2]) == 'OK':
                cli = Client(base_url='tcp://'+host[1]+':'+str(host[2]))
                platform = cli.info()
                containers = cli.containers(all=True, trunc=True)
                container = sorted(containers, key=lambda x: x['Names'])

                vcontainer = str(container).replace('\'','\'\'')
                vplatform = str(platform).replace('\'','\'\'')

                _execute("UPDATE hosts SET api_container='%s', api_platform='%s', status='%s' where ip='%s'" % (vcontainer,vplatform,'OK',host[1]))

            else:
                _execute("UPDATE hosts SET status='%s' where ip='%s'" % ('FAIL',host[1]))
                pass

        cli = Client(base_url='tcp://192.168.0.103:3375')
        platform = cli.info()

        machine_info = {
            "KernelMI":platform['KernelVersion'],
            "OSMI":platform['OperatingSystem'],
            "HostsMI":platform['Name'],
            "ProcessorMI":platform['NCPU'],
            "TotalSI":size(psutil.swap_memory()[0]),
            "UsedSI":size(psutil.swap_memory()[0]),
            "FreeSI":size(psutil.swap_memory()[2]),
            "TotalMI":size(psutil.virtual_memory()[0]),
            "UsedMI":size(psutil.virtual_memory()[3]),
            "FreeMI":size(psutil.virtual_memory()[4]),
        }

        data = dict()
        data_cont = dict()
        data_catgory = _execute("SELECT name FROM category where id = %s" % (argument))
        for host in data_hosts:
            hostip = host[1]
            hostport = host[2]
            hostip_container = str(hostip)+'-Container'
            hostip_platform = str(hostip)+'-Platform'

            data_container = _execute("SELECT api_container FROM hosts WHERE ip='%s'" % (hostip))
            container = ast.literal_eval(str(data_container[0][0]))

            data_platform = _execute("SELECT api_platform FROM hosts WHERE ip='%s'" % (hostip))
            platform = ast.literal_eval(data_platform[0][0])

            data.update({hostip_container:container})
            data.update({hostip_platform:platform})

        for data_host in data_hosts:
            ip = data_host[1]
            port = str(data_host[2])
            for container in range(len(data[data_host[1]+'-Container'])):
                container_name = str(data[data_host[1]+'-Container'][container]['Names'][0]).replace('/','')
                container_id = str(data[data_host[1]+'-Container'][container]['Id']).replace('/','')

                if data_host[4] == 'OK':
                   cli = Client(base_url='tcp://'+ip+':'+port)
                   platform = cli.info()

                   container = cli.inspect_container(container_id)
                   vcontainer = str(container).replace('\'','\'\'')

                   vplatform = str(platform).replace('\'','\'\'')

                   logs_out = (cli.logs(container_name,tail=50).decode('utf-8'))
                   logs_value = multiple_replace(logs_out, repls)

                   vlogs_value = str(logs_value).replace('\'','\'\'')
                   vlogs_value = str(vlogs_value).replace('\"','\'\'')

                   _execute("UPDATE container SET api_container='%s', api_platform='%s', logs_value='%s' where container_id='%s'" % (vcontainer,vplatform,vlogs_value,container_id))
                else:
                   pass

                data_container = _execute("SELECT * FROM container WHERE ip='%s' AND container='%s'" % (ip,container_name))
                if not list(data_container):
                    _execute("INSERT INTO container (ip,container,status,port,pid,container_id) VALUES ('%s','%s','%s','%s','%s','%s')" % (ip,container_name,'deactive',0,0,container_id))
                else:
                    pass

        data_container = _execute("SELECT ip,container,status,port FROM container")
        for containers in data_container:
            ipcontainer = containers[1]+"ip="+containers[0]
            container = containers[1]
            status = containers[2]
            port = containers[3]
            data_cont.update({ipcontainer:[status,port]})

        self.render("filter.html",machine_info=machine_info,data_hosts=data_hosts,data=data,data_cont=data_cont,data_catgory=data_catgory)

class StartHandler(tornado.web.RequestHandler):
    def get(self, argument):
        data = argument.rsplit('/', 1)[-1]
        container = data.rsplit('ip=',1)[0]
        ip = data.rsplit('ip=',1)[-1]

        data_port = _execute("SELECT port FROM hosts where ip='%s'" % (ip))
        try:
            cli = Client(base_url='tcp://'+ip+':'+str(data_port[0][0]))
            cli.start(container)
            self.write("<script>location.replace(document.referrer);</script>")
        except Exception:
            self.write("<script>alert('Error (Tolong untuk di jalankan secara manual !!)'); location.replace(document.referrer); </script>")

class StopHandler(tornado.web.RequestHandler):
    def get(self, argument):
        data = argument.rsplit('/', 1)[-1]
        container = data.rsplit('ip=',1)[0]
        ip = data.rsplit('ip=',1)[-1]
        data_port = _execute("SELECT port FROM hosts where ip='%s'" % (ip))
        try:
            cli = Client(base_url='tcp://'+ip+':'+str(data_port[0][0]))
            cli.stop(container)
            self.write("<script>location.replace(document.referrer);</script>")
        except Exception:
            self.write("<script>alert('Error (Tolong untuk di berhentikan secara manual !!)'); location.replace(document.referrer); </script>")

class AttachHandler(tornado.web.RequestHandler):
    def get(self, argument):
        data = argument.rsplit('/', 1)[-1]
        container = data.rsplit('ip=',1)[0]
        ip = data.rsplit('ip=',1)[-1]

        try:
            data_container = _execute("SELECT status,port,pid FROM container WHERE ip='%s' AND container='%s'" % (ip,container))
            if data_container:
                if data_container[0][0] == 'deactive' and data_container[0][1] == 0 and data_container[0][2] == 0:
                       number=8000
                       status=False
                       while status == False:
                            number+=1
                            if str(number) in scann_port.main():
                                status=False
                                pass
                            else:

                                data_host_port = _execute("SELECT port FROM hosts WHERE ip='%s'" % (ip))
                                _execute("UPDATE container SET status='active' WHERE ip='%s' and container='%s'" % (ip,container))

                                status=True
                                cmd = ['/usr/bin/env','python3.5',options.root_path+'/tupay/tupay.server.py','--host='+options.listen_address,'--port='+str(number),'--ip_machine='+ip,'--port_machine='+str(data_host_port[0][0]),'--container_name='+container,'>>','terminal_log','2>&1','&']
                                os.system(' '.join(cmd))
                                time.sleep(6)
                                self.write("<script>location.replace(document.referrer);</script>")
                                
                       _execute("UPDATE container SET port='%s' WHERE ip='%s' and container='%s'" % (number,ip,container))
                else:
                     data_pid = _execute("SELECT pid FROM container WHERE ip='%s' AND container='%s'" % (ip,container))
                     if data_pid[0][0] == 0:
                        _execute("UPDATE container SET status='deactive', port='0' WHERE ip='%s' and container='%s'" % (ip,container))
                        self.write("<script>location.replace(document.referrer);</script>") 
                     else:
                        pids = 'kill -9 '+str(data_pid[0][0])
                        os.system(pids)
                        _execute("UPDATE container SET status='deactive', port='0', pid='0' WHERE ip='%s' and container='%s'" % (ip,container))
                        self.write("<script>location.replace(document.referrer);</script>")

            else:
                self.write("<script>alert('Error (Tidak dapat memproses ke data container !!)'); location.replace(document.referrer);</script>")

        except Exception:
            self.write("<script>alert('Error (Tolong attach/exec container secara manual !!)'); location.replace(document.referrer);</script>")    


class DetailHandler(tornado.web.RequestHandler):
    def get(self, data):
        name_container = data.rsplit('ip=',1)[0]
        data_ip = data.rsplit('ip=',1)[-1]
        ip = data_ip.rsplit('container_id=',1)[0]
        container_id = data_ip.rsplit('container_id=',1)[-1]

        data_container = _execute("SELECT api_container FROM container WHERE ip='%s' and container_id='%s'" % (ip,container_id))
        container = ast.literal_eval(str(data_container[0][0]))

        data_platform = _execute("SELECT api_platform FROM container WHERE ip='%s' and container_id='%s'" % (ip,container_id))
        platform = ast.literal_eval(data_platform[0][0])

        data_logs = _execute("SELECT logs_value FROM container WHERE ip='%s' and container_id='%s'" % (ip,container_id))
        logs_out = data_logs[0][0]

        logs_value = multiple_replace(logs_out, repls)
        self.render("detail.html",container=container,platform=platform,ip=ip,name_container=name_container,container_id=container_id,logs_value=logs_value)

class ErrorHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("404.html")

class CreateHandler(tornado.web.RequestHandler):
    def get(self):
        data_category = _execute("SELECT id,name FROM category")
        self.render("create.html",data_category=data_category)

    def post(self):
        data_category = _execute("SELECT id,name FROM category")
        ip = self.get_argument("ip")
        port = self.get_argument("port")
        details = self.get_argument("details")
        if ip == "" or str(port) == "" or details == "":
            self.write("<script>alert('Error (Silahkan lengkapi data !!)'); location.replace(document.referrer); </script>")
        else:
            data_check = _execute("SELECT id FROM hosts where ip='%s'" % (ip,))
            if data_check:
               self.write("<script>alert('IP sudah terdaftar !!');location.replace(document.referrer);</script>")
            else:
               if re.match(".*[\%\$\^\*\@\!\_\-\(\)\:\;\'\"\{\}\[\]].*", ip) or re.match(".*[\%\$\^\*\@\!\_\-\(\)\:\;\'\"\{\}\[\]].*", port) or re.match(".*[\%\$\^\*\@\!\_\-\(\)\:\;\'\"\{\}\[\]].*", details):
                  self.write("<script>alert('Error (Invalid special characters !!)'); location.replace(document.referrer); </script>")
               else:
                  if healthcheck(ip,port) == 'OK':
                     _execute("INSERT INTO hosts (ip,port,details) VALUES ('%s','%s','%s')" % (ip,port,details))

                     self.write("<script>alert('Host berhasil ditambahkan silahkan masuk ke mesin tupay.detik.com (103.49.220.200) dan masukan command berikut : ssh-copy-id root@"+ ip +",  !!')</script>")
                     self.render("create.html",data_category=data_category)
                  else:
                     self.write("<script>alert('Port & IP tidak terhubung !!');location.replace(document.referrer);</script>")  

class CreateCategoryHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("category.html")
    def post(self):
        details = self.get_argument("details")
        if details == "":
            self.write("<script>alert('Error (Tolong lengkapi data !!)'); location.replace(document.referrer); </script>")
        else:
            if re.match(".*[\%\$\^\*\@\!\_\-\(\)\:\;\'\"\{\}\[\]].*", details):
                self.write("<script>alert('Error (Spesial karakter tidak diijinkan !!)'); location.replace(document.referrer); </script>")
            else:
                _execute("INSERT INTO category (name) VALUES ('%s')" % (details))
                self.write("<script>alert('Kategori berhasil ditambahkan !!')</script>")
                self.redirect("/create")


class DeleteCategoryHandler(tornado.web.RequestHandler):
    def get(self,ids):
        details = self.get_argument("details")
        _execute("DELETE FROM category WHERE name = %s",details)
        self.redirect("/category")

class DeleteHandler(tornado.web.RequestHandler):
    def get(self,ids):
        host_ip = _execute("SELECT ip FROM hosts WHERE id='%s'" % (ids))
        _execute("DELETE FROM hosts WHERE id='%s'" % (ids))
        _execute("DELETE FROM container where ip='%s'" % (host_ip[0][0]))

        self.write("<script>location.replace(document.referrer);</script>")  

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port,address=options.ip)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
